A command line importer using Proteus to populate a new Tryton db from CSV values.
Built for Tryton 5.0 (see tryton.org)
Shared as is, probably you will need to tweak it depending on the modules you will activate.
