# -*- coding: utf-8 -*-

CONFIGURE_DATABASE = {
    'account_template': u'Plan comptable (French)',
    'company': {
        'name': u'My Compagny',
        'address': {
            'name': u'',
            'street': u'Street',
            'zip': u'01234',
            'city': u'Town',
        },
        'country': u'FR',
        'currency': u'EUR',
        'footer': u'My compagny, Street Town' + \
            'SIRET 000 000 000 00001 • TVA FR000000000',
        'lang': u'fr',
        'timezone': u'Europe/Paris',
        'vat_number': u'FR000000000',
        'siren': u'000000000',
        'siret_nic': u'00001'
    },
    'users': [
        {   'id': 'me',
            'email': 'me@example.com',
            'name': u'My NAME',
            'password': u'xxxxxxxxxxxxxxxxx',
            'employee': True,
        },
        {   'id': 'him',
            'email': 'him@example.com',
            'name': u'His NAME',
            'password': u'xxxxxxxxxxxxxxxxx',
            'employee': True,
        },
    ],
    'modules': [
        'account_fr',
        'account_invoice',
        'account_invoice_history',
        'account_statement',
        'account_stock_continental',
        'analytic_account',
        'analytic_invoice',
        'analytic_purchase',
        'analytic_sale',
        'carrier',
        'carrier_weight',
        'sale_price_list',
        'party_relationship',
        'party_siret',
        'product_measurements',
        'purchase',
        'sale_shipment_cost',
    ],
    'sequences': [
        {   'use': 'in_invoice',
            'name': u'Factures fournisseur',
            'prefix': u'${year}',
            'padding': 5
        },
        {   'use': 'in_credit_note',
            'name': u'Notes de crédit fournisseur',
            'prefix': u'NC${year}',
            'padding': 5
        },
        {   'use': 'out_invoice',
            'name': u'Factures client',
            'prefix': u'${year}',
            'padding': 5
        },
        {   'use': 'out_credit_note',
            'name': u'Notes de crédit client',
            'prefix': u'${year}',
            'padding': 5
        }
    ]
}
